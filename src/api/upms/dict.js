import request from '@/router/axios'

export function fetchList (query) {
  return request({
    url: '/upms/upmsDict/page',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: '/upms/upmsDict/',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: '/upms/upmsDict/' + id,
    method: 'get'
  })
}

export function delObj (row) {
  return request({
    url: '/upms/upmsDict/' + row.id + '/' + row.type,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: '/upms/upmsDict/',
    method: 'put',
    data: obj
  })
}

export function remote (type) {
  return request({
    url: '/upms/upmsDict/type/' + type,
    method: 'get'
  })
}

