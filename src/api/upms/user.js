import request from '@/router/axios';

export const getUserInfo = () => {
  return request({
    url: '/upms/upmsUser/info',
    method: 'get'
  })
}


export function fetchList (query) {
  return request({
    url: '/upms/upmsUser/page',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: '/upms/upmsUser',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: '/upms/upmsUser/' + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: '/upms/upmsUser/' + id,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: '/upms/upmsUser',
    method: 'put',
    data: obj
  })
}

