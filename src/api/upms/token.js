import request from '@/router/axios';


export function fetchList (query) {
  return request({
    url: '/upms/upmsToken/page',
    method: 'get',
    params: query
  })
}

export function delObj (token) {
  return request({
    url: '/upms/upmsToken/' + token,
    method: 'delete'
  })
}



