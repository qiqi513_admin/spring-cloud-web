import request from '@/router/axios';


export function getUserTree (query) {
  return request({
    url: '/upms/upmsDept/getUserTree',
    method: 'get',
    params: query
  })
}

export function fetchTree (query) {
  return request({
    url: '/upms/upmsDept/tree',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: '/upms/upmsDept/',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: '/upms/upmsDept/' + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: '/upms/upmsDept/' + id,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: '/upms/upmsDept/',
    method: 'put',
    data: obj
  })
}


