import request from '@/router/axios';


export function fetchList (query) {
  return request({
    url: '/upms/upmsClient/page',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: '/upms/upmsClient/',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: '/upms/upmsClient/' + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: '/upms/upmsClient/' + id,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: '/upms/upmsClient',
    method: 'put',
    data: obj
  })
}



