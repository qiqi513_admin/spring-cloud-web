import request from '@/router/axios';

export function getRoleList (query) {
  return request({
    url: '/upms/upmsRole/list',
    method: 'get',
    params: query
  })
}


export function fetchList (query) {
  return request({
    url: '/upms/upmsRole/page',
    method: 'get',
    params: query
  })
}


export function getObj (id) {
  return request({
    url: '/upms/upmsRole/' + id,
    method: 'get'
  })
}

export function addObj (obj) {
  return request({
    url: '/upms/upmsRole',
    method: 'post',
    data: obj
  })
}

export function putObj (obj) {
  return request({
    url: '/upms/upmsRole',
    method: 'put',
    data: obj
  })
}

export function delObj (id) {
  return request({
    url: '/upms/upmsRole/' + id,
    method: 'delete'
  })
}


export function permissionUpd (roleId, menuIds) {
  return request({
    url: '/upms/upmsRole/menu',
    method: 'put',
    params: {
      roleId: roleId,
      menuIds: menuIds
    }
  })
}

export function fetchRoleTree (roleName) {
  return request({
    url: '/upms/upmsRole/tree/' + roleName,
    method: 'get'
  })
}

