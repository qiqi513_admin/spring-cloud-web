import request from '@/router/axios';

export function fetchList (query) {
  return request({
    url: '/upms/upmsLog/page',
    method: 'get',
    params: query
  })
}

export function delObj (id) {
  return request({
    url: '/upms/upmsLog/' + id,
    method: 'delete'
  })
}
export function sendLogs (logsList) {
  return request({
    url: '/upms/upmsLog/logs',
    method: 'post',
    data: logsList
  })
}

