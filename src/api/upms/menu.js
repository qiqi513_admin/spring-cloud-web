
import request from '@/router/axios'

export function getMenu() {
  return request({
    url: '/upms/upmsMenu/getUserMenu',
    method: 'get'
  })
}

export function fetchMenuTree (query) {
  return request({
    url: '/upms/upmsMenu/tree',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: '/upms/upmsMenu',
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: '/upms/upmsMenu/' + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: '/upms/upmsMenu/' + id,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: '/upms/upmsMenu',
    method: 'put',
    data: obj
  })
}


