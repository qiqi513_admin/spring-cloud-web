export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  editBtn: false,
  delBtn: false,
  align: 'center',
  addBtn: false,
  column: [
      {
          fixed: true,
          label: 'id',
          prop: 'userId',
          span: 12,
          hide: true,
          editDisabled: true,
          addVisdiplay: false
      },
      {
          fixed: true,
          label: '用户名',
          prop: 'username',
          editDisabled: true,
          search: true,
          span: 12,
          rules: [
            { required: true,message: '请输入用户名'},
            { min: 3,max: 20,message: '长度在 3 到 20 个字符',trigger: 'blur'}
          ]
      },
      {
          label: '密码',
          prop: 'password',
          type: 'password',
          value: '',
          hide: true,
          span: 12,
          rules: [
              {  min: 6,max: 20, message: '长度在 6 到 20 个字符',rigger: 'blur' }
          ]
      },
      {
          label: '所属部门',
          prop: 'deptId',
          slot: true,
          span: 12,
          hide: true,
          formslot: true,
          rules: [
              { required: true,  message: '请选择部门', trigger: 'blur'  }
          ]
      },
      {
          label: '手机号',
          prop: 'phone',
          type: 'iphone',
          span: 12,
          rules: [
              { min: 6, max: 20, message: '长度在 11 个字符', trigger: 'blur' }
          ]
      },
      {
          label: '角色',
          prop: 'role',
          // formslot: true,
          slot: true,
          overHidden: true,
          type: 'select',
          props: {
              label: "roleName",
              value: 'roleId'
          },
          dicUrl:'/upms/upmsRole/list',
          multiple:true,
          span: 12,
          rules: [
              { required: true,message: '请选择角色',trigger: 'blur' }
          ]
      },
      {
          label: '状态',
          prop: 'lockFlag',
          type: 'select',
          // slot: true,
          span: 12,
          rules: [
              {required: true,message: '请选择状态',trigger: 'blur' }
          ],
          dicData: [
              {label: '有效',value: '0'},
              {label: '锁定',value: '9'}
          ]
      },
      {
          width: 180,
        label: '创建时间',
        prop: 'createTime',
        type: 'datetime',
        format: 'yyyy-MM-dd HH:mm',
        valueFormat: 'yyyy-MM-dd HH:mm:ss',
        editDisabled: true,
        addVisdiplay: false,
        span: 12
    }
  ]
}
